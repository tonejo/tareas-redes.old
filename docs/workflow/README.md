# Flujo de trabajo para la entrega de tareas

||
|:----:|
|![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")|

## Acceder al repositorio principal

+ Abrir la URL del [repositorio de tareas para la materia][repositorio-tareas]

||
|:----:|
|![](img/001-Main_repo.png "")

+ [Iniciar sesión en GitLab][gitlab-login]

||
|:----:|
|![](img/002-Sign_in.png "")

## Hacer `fork` al repositorio principal

+ Dar clic en el botón de `fork` para crear una copia del repositorio `tareas-redes`  en tu cuenta de usuario

||
|:----:|
|![](img/003-Fork_repo.png "")

+ Esperar a que el __fork__ se complete

||
|:----:|
|![](img/004-Fork_in_progress.png "")

## Clonar el repositorio

+ Accede a la URL del repositorio `tareas-redes` asociado a **tu cuenta de usuario**

+ Obten la URL de tu repositorio `tareas-redes` y bájalo a tu equipo con `git clone`:

||
|:----:|
|![](img/005-Fork_successful-clone_URL.png "")

```
$ git clone https://gitlab.com/USUARIO/tareas-redes.git
Cloning into 'tareas-redes'...
remote: Enumerating objects: 24, done.
remote: Counting objects: 100% (24/24), done.
remote: Compressing objects: 100% (22/22), done.
remote: Total 140 (delta 7), reused 0 (delta 0), pack-reused 116
Receiving objects: 100% (140/140), 1021.15 KiB | 2.49 MiB/s, done.
Resolving deltas: 100% (13/13), done.
Checking connectivity... done.

$ cd tareas-redes/
$ ls -lA
total 36
drwxrwsr-x 8 tonejito users 4096 ene 28 15:52 .git
-rw-rw-r-- 1 tonejito users    8 ene 28 15:47 .gitignore
-rw-rw-r-- 1 tonejito users  319 ene 28 15:47 .gitlab-ci.yml
drwxrwsr-x 4 tonejito users 4096 ene 28 15:47 content
drwxrwsr-x 2 tonejito users 4096 ene 28 15:47 static
drwxrwsr-x 4 tonejito users 4096 ene 28 15:47 themes
-rw-rw-r-- 1 tonejito users  408 ene 28 15:47 README.md
-rw-rw-r-- 1 tonejito users 1078 ene 28 15:47 LICENSE
-rw-rw-r-- 1 tonejito users 1236 ene 28 15:47 config.toml

$ tree -a content/
content/
├── _index.md
├── entrega
│   ├── .gitkeep
│   └── _index.md
└── post
    ├── 2019-08-05-tareas.md
    └── _index.md
```

## Crear rama de trabajo

+ Revisa que de manera inicial te encuentres en la rama `entregas`

```
$ git branch
* entregas
```

+ Crea una rama con tu nombre utilizando `git checkout`

```
$ git checkout -b AndresHernandez
Switched to a new branch 'AndresHernandez'
```

+ Revisa que hayas cambiado a la rama con tu nombre

```
$ git branch
* AndresHernandez
  entregas
```

## Agregar carpeta personal

+ Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `content/entrega`

```
$ mkdir -vp content/entrega/AndresHernandez/
mkdir: created directory ‘content/entrega/AndresHernandez/’
```

>>>
Dentro de esa carpeta es donde debes poner los archivos de las tareas que hagas
>>>

### Agregar archivo con tu nombre

+ Crea el archivo `.gitkeep` y un archivo de Markdown que contenga tu nombre

```
$ touch  content/entrega/AndresHernandez/.gitkeep
$ editor content/entrega/AndresHernandez/README.md
```

+ Contenido de ejemplo para el archivo README.md:

```
---
title: NOMBRE COMPLETO
subtitle: NÚMERO DE CUENTA
date: 2020-02-28
comments: false
---

Hola

Esta es la página de NOMBRE COMPLETO
```

## Enviar cambios al repositorio

+ Una vez que hayas creado los archivos, revisa el estado del repositorio

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	content/entrega/AndresHernandez/

nothing added to commit but untracked files present (use "git add" to track)
```

+ Cambia de directorio y agrega los archivos con `git add`

```
$ git add content/entrega/AndresHernandez/
```

+ Revisa que los archivos hayan sido agregados utilizando `git status`

```
$ git status
On branch entregas
Your branch is up to date with 'origin/entregas'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   content/entrega/AndresHernandez/.gitkeep
	new file:   content/entrega/AndresHernandez/README.md

```

+ Versiona los archivos con `git commit`

```
$ git commit
[AndresHernandez 728be93] Carpeta de Andrés Hernández
 Author: Andrés Hernández <tonejito@tonejito.org>
 Date: Tue Jan 28 17:30:00 2020 -0600
 2 files changed, 10 insertions(+)
 create mode 100644 content/entrega/AndresHernandez/.gitkeep
 create mode 100644 content/entrega/AndresHernandez/README.md
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```
$ git push -u origin AndresHernandez
Counting objects: 6, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 679 bytes | 679.00 KiB/s, done.
Total 6 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote: 
To https://gitlab.com/USUARIO/tareas-redes.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```

## Crea un `merge request` para entregar tu tarea

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url de **tu repositorio**:

```
https://gitlab.com/USUARIO/tareas-redes
```

||
|:----:|
|![](img/006-Fork-commit_ok.png "")

+ Ve a la sección llamada `merge requests` en la barra lateral

+ Crea un nuevo `merge request` para enviar los cambios al repositorio central

||
|:----:|
|![](img/007-Fork-new_MR.png "")

+ Selecciona la rama `entregados` como __destino__

||
|:----:|
|![](img/008-Fork-MR_target_branch.png "")

+ Crea el `merge request` utilizando la siguiente pantalla como plantilla

||
|:----:|
|![](img/009-Fork-MR_data.png "")

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

||
|:----:|
|![](img/009-Main_MR_merged.png "")

--------------------------------------------------------------------------------

>>>
**Protip**
Puedes cambiar la visibilidad del proyecto a privada

+ En la barra lateral seleccionar `Settings` > `General`
+ Expande la parte llamada `Permissions`, ahí puedes seleccionar la visibilidad deseada
+ Da clic en `Save changes` para aplicar
>>>

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2020-2/tareas-redes
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-redes
